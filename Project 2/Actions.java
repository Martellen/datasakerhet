import java.util.ArrayList;
import java.util.stream.Collectors;

public class Actions {
    private static DataBase database;
    private static User currentUser;
    private static AuditLog log;

    public Actions(DataBase database, User currentUser, AuditLog log) {
        this.database = database;
        this.currentUser = currentUser;
        this.log = log;
    }

    public static String performAction (String input) {
        try {
            String[] commands = input.split(" ");
            int id = 0;

            switch (commands[0].toLowerCase()) {
                // case "readall":
                //     return readAll();

                case "help":
                    return help();

                case "read":
                    id = Integer.parseInt(commands[1]);
                    return readId(id);

                case "write":
                    id = Integer.parseInt(commands[1]);
                    StringBuilder sb = new StringBuilder();
                    for (int i = 2; i < commands.length; i++) {
                        sb.append(commands[i] + " ");
                    }
                    return write(sb.toString(), id);

                case "create":
                    String nurseAlias = commands[1];
                    String patientAlias = commands[2];

                    User nurse = database.getUser(nurseAlias);
                    User patient = database.getUser(patientAlias);

                    StringBuilder sb2 = new StringBuilder();
                    for (int i = 3; i < commands.length; i++) {
                        sb2.append(commands[i] + " ");
                    }
                    System.out.println(sb2.toString());
                    return create(nurse, patient, sb2.toString());

                case "delete":
                    id = Integer.parseInt(commands[1]);
                    return delete(id);

                default:
                    return "Wrong input";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Wrong input";
        }
    }

    private static String help() {
        return "The commands are the following: read, help, write, create, delete";
    }

    // funktioner for varje case som kollar om man har authority
    private static String readId (int id) {
        MedicalRecord patientRec = database.getRecord(id);
        if (patientRec != null) {
            int privilege = checkPrivilege(patientRec);
            if (privilege >= 1) {
                log(id, "read", "allowed");
                return patientRec.toString();
            }
            log(id, "read", "denied");
            return "Not authorized";
        }
        log(id, "read", "denied");
        return "Record doesn't exist";
    }

    private static String write (String notes, int id) {
        MedicalRecord patientRec = database.getRecord(id);
        int privilege = checkPrivilege(patientRec);
        if (privilege == 2) {
            patientRec.appendNotes(notes);
            database.writeRecords();
            log(id, "write", "allowed");
            return "Successfully added";
        } else {
            log(id, "write", "denied");
            return "Not authorized";
        }
    }

    private static String delete (int id) {
        MedicalRecord patientRec = database.getRecord(id);
        if (patientRec != null) {
            int privilege = checkPrivilege(patientRec);
            if (privilege >= 3) {
                if (database.removeRec(id)) {
                    database.writeRecords();
                    log(id, "delete", "allowed");
                    return "Successfully deleted";
                }
            }
            log(id, "delete", "denied");
            return "Not authorized";
        } else {
            log(id, "delete", "doesn't exist");
            return "Record doesn't exist";
        }
    }

    private static String create(User nurse, User patient, String notes) {
        if (currentUser.getRole().equals("doctor")) {
            MedicalRecord rec = new MedicalRecord(currentUser, nurse, patient, currentUser.getDivision(),database.getMaxId()+1,notes);
            database.addRecord(rec);
            database.writeRecords();
            int id = database.getMaxId();
            log(id, "create", "allowed");
            return "Successfully added new record";
        }
        log(-1, "create", "allowed");
        return "Not authorized";
    }

    private static int checkPrivilege(MedicalRecord record) {
        if (currentUser.getRole().equals("agency")) {
            return 3;
        } else if (currentUser.getAlias().equals(record.getNurse().getAlias()) || currentUser.getAlias().equals(record.getDoctor().getAlias())) {
            return 2;
        } else if (currentUser.getAlias().equals(record.getPatient().getAlias()) || (currentUser.getDivision().equals(record.getDivision()) && !currentUser.getRole().equals("patient"))) {
            return 1;
        } else {
            return -1;
        }
    }

    private static void log(int id, String action, String status) {
        if (id == -1) {
            log.logging("Name: " + currentUser.getName() + " | alias: " + currentUser.getAlias() + " | action: " + action + " | status: " + status);
        } else {
            log.logging("Name: " + currentUser.getName() + " | alias: " + currentUser.getAlias() + " | action: " + action + " | record: " + id + " | status: " + status);
        }
    }
}
