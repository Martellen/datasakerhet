# Start Server & Client with:
java Server 9876

java Client localhost 9876

Database structure is
alias;password;division;role;full name

Records structure is
doctor alias;nurse alias;patient alias;division;id;notes

# User Interface

## Commands: help, read, write, create, delete

### read
read 'record  id'

### write
write 'record id' 'notes'

### create
create 'nurse alias' 'patient alias' 'notes'

### delete
delete 'record id'
