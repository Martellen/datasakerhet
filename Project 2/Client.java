import java.io.*;
import javax.net.ssl.*;
import java.security.cert.X509Certificate;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.util.Base64;
import java.util.Enumeration;
import java.security.cert.Certificate;
import javax.net.ssl.SSLSession;

/*
 * This example shows how to set up a key manager to perform client
 * authentication.
 *
 * This program assumes that the client is not inside a firewall.
 * The application can be modified to connect to a server outside
 * the firewall by following SSLSocketClientWithTunneling.java.
 */
public class Client {
  public static void main(String[] args) throws Exception {
    String host = null;
    int port = -1;
    for (int i = 0; i < args.length; i++) {
      System.out.println("args[" + i + "] = " + args[i]);
    }
    if (args.length < 2) {
      System.out.println("USAGE: java client host port");
      System.exit(-1);
    }
    try { /* get input parameters */
      host = args[0];
      port = Integer.parseInt(args[1]);
    } catch (IllegalArgumentException e) {
      System.out.println("USAGE: java client host port");
      System.exit(-1);
    }

    try {
      SSLSocketFactory factory = null;
      String alias = "";
      try {
        char[] password = "password".toCharArray();
        KeyStore ks = KeyStore.getInstance("JKS");
        KeyStore ts = KeyStore.getInstance("JKS");
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
        SSLContext ctx = SSLContext.getInstance("TLSv1.2");
        ks.load(new FileInputStream("util/clientkeystore"), password);

        /*
         * Asks user for valid alias (certificate) in keystore and removes all other entries
         */
        Enumeration<String> aliases = ks.aliases();
        while (true) {
          alias = System.console().readLine("Enter alias (username): ");
          if (ks.containsAlias(alias)) {
            System.out.println("\nAlias found: " + alias + "\n");
            break;
          } else {
            System.out.println("\nAlias not found - try again\n");
          }
        }
        while (aliases.hasMoreElements()) {
          String a = aliases.nextElement();
          if (!a.equals(alias) && a != null) {
            ks.deleteEntry(a);
            aliases = ks.aliases();
          }
        }

        ts.load(new FileInputStream("util/serverkeystore"), password);
        kmf.init(ks, password); // user password (keypass)
        tmf.init(ts); // keystore can be used as truststore here
        ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
        factory = ctx.getSocketFactory();
      } catch (Exception e) {
        throw new IOException(e.getMessage());
      }
      SSLSocket socket = (SSLSocket) factory.createSocket(host, port);

      /*
       * send http request
       *
       * See SSLSocketClient.java for more information about why
       * there is a forced handshake here when using PrintWriters.
       */
      socket.startHandshake();
      // SSLSession session = socket.getSession();
      // Certificate[] chain = session.getPeerCertificates();          
      //String serverCN = ((X509Certificate) chain[0]).getSubjectX500Principal().getName();
      //System.out.println("Certificate received from server: " + serverCN + "\n");

      BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
      PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
      BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

      // Send alias to server
      out.println(alias);
      out.flush();

      String msg = null;
      while ((msg = in.readLine()) != null) {
        System.out.println(msg);
        msg = null;
        break;
      }

      /*
       * Asks user for password and sends it to server, unlimited tries until correct password is entered
       */
      Console console = System.console();
      boolean quit = false;
      while (!quit) {
        String password = new String(console.readPassword("Password: "));
        String hashedPassword = hashPassword(password);
        out.println(hashedPassword);
        out.flush();
        while ((msg = in.readLine()) != null) {
          System.out.println(msg);
          if (msg.contains("incorrect")) {
            msg = null;
            break;

          } else {
            msg = null;
            quit = true;
            break;
          }
        }
      }
      
      // Temorary to generate passwords
      // String[] users = {"maria", "anna", "johan", "anders", "lisa", "sven", "eva", "jonas", "malin", "socialstyrelsen"};
      // String[] passwords = new String[10];
      // for (int i = 0; i < 10; i++) {
      //   passwords[i] = hashPassword(users[i] + "password");
      // }
      // for (int i = 0; i < 10; i++) {
      //   System.out.println(users[i] + " " + passwords[i]);
      // }

      for (;;) {
        System.out.print("Enter command:>");
        msg = read.readLine();
        if (msg.equalsIgnoreCase("quit")) {
          break;
        }
        System.out.print("sending '" + msg + "' to server...");
        out.println(msg);
        out.flush();
        System.out.println("done");
        System.out.println("received '" + in.readLine() + "' from server\n");
      }
      in.close();
      out.close();
      read.close();
      socket.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static String hashPassword(String password) {
    try {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        byte[] hashedBytes = messageDigest.digest(password.getBytes());
        return Base64.getEncoder().encodeToString(hashedBytes);
    } catch (Exception e) {
        System.out.println("Error hashing password: " + e.getMessage());
        return null;
    }
}
}
