# CA certificate
openssl req -x509 -sha256 -nodes -days 365 -newkey RSA -keyout ca.key -out ca.crt -subj "/CN=CA"

# Import CA certificate into clienttruststore
keytool -import -alias ca -file ca.crt -keystore clienttruststore -storepass password

# Generate a new key-pair for the client and store it in clientkeystore
# Generate keys for three nurses
keytool -genkey -alias maria_olsson -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Maria Olsson, OU=ONH, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"
keytool -genkey -alias anna_berg -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Anna Berg, OU=ONH, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"
keytool -genkey -alias johan_svensson -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Johan Svensson, OU=ONH, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"

# Generate keys for three patients
keytool -genkey -alias anders_andersson -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Anders Andersson, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"
keytool -genkey -alias lisa_lindgren -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Lisa Lindgren, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"
keytool -genkey -alias sven_svensson -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Sven Svensson, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"

# Generate keys for three doctors
keytool -genkey -alias eva_johansson -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Eva Johansson, OU=Oncology, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"
keytool -genkey -alias jonas_gustavsson -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Jonas Gustavsson, OU=Cardiology, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"
keytool -genkey -alias malin_larsson -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Malin Larsson, OU=Pediatrics, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"

# Generate key for Socialstyrelsen
keytool -genkey -alias socialstyrelsen -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Socialstyrelsen, ST=Sweden, C=SE"


# Generate a certificate signing request for the clients public key and store it in clientkeystore
keytool -certreq -alias maria_olsson -keystore clientkeystore -file Maria_Olsson.csr -storepass password 
keytool -certreq -alias anna_berg -keystore clientkeystore -file Anna_Berg.csr -storepass password 
keytool -certreq -alias johan_svensson -keystore clientkeystore -file Johan_Svensson.csr -storepass password 

keytool -certreq -alias eva_johansson -keystore clientkeystore -file Eva_Johansson.csr -storepass password
keytool -certreq -alias jonas_gustavsson -keystore clientkeystore -file Jonas_Gustavsson.csr -storepass password 
keytool -certreq -alias malin_larsson -keystore clientkeystore -file Malin_Larsson.csr -storepass password

keytool -certreq -alias anders_andersson -keystore clientkeystore -file Anders_Andersson.csr -storepass password
keytool -certreq -alias lisa_lindgren -keystore clientkeystore -file Lisa_Lindgren.csr -storepass password
keytool -certreq -alias sven_svensson -keystore clientkeystore -file Sven_Svensson.csr -storepass password

keytool -certreq -alias socialstyrelsen -keystore clientkeystore -file Socialstyrelsen.csr -storepass password

#Generate a digital certificate for the clients public key using the CSR§
echo 01 > ca.srl
openssl x509 -req -in maria_olsson.csr -CA ca.crt -CAkey ca.key -out Maria_Olsson.crt -CAcreateserial -days 365
openssl x509 -req -in anna_berg.csr -CA ca.crt -CAkey ca.key -out Anna_Berg.crt -CAcreateserial -days 365
openssl x509 -req -in johan_svensson.csr -CA ca.crt -CAkey ca.key -out Johan_Svensson.crt -CAcreateserial -days 365

openssl x509 -req -in eva_johansson.csr -CA ca.crt -CAkey ca.key -out Eva_Johansson.crt -CAcreateserial -days 365
openssl x509 -req -in jonas_gustavsson.csr -CA ca.crt -CAkey ca.key -out Jonas_Gustavsson.crt -CAcreateserial -days 365
openssl x509 -req -in malin_larsson.csr -CA ca.crt -CAkey ca.key -out Malin_Larsson.crt -CAcreateserial -days 365

openssl x509 -req -in anders_andersson.csr -CA ca.crt -CAkey ca.key -out Anders_Andersson.crt -CAcreateserial -days 365
openssl x509 -req -in lisa_lindgren.csr -CA ca.crt -CAkey ca.key -out Lisa_Lindgren.crt -CAcreateserial -days 365
openssl x509 -req -in sven_svensson.csr -CA ca.crt -CAkey ca.key -out Sven_Svensson.crt -CAcreateserial -days 365

openssl x509 -req -in socialstyrelsen.csr -CA ca.crt -CAkey ca.key -out Socialstyrelsen.crt -CAcreateserial -days 365

# Import certificate chain into clientkeystore
keytool -import -alias ca -file ca.crt -keystore clientkeystore -storepass password
keytool -import -alias maria_olsson -file Maria_Olsson.crt -keystore clientkeystore -storepass password
keytool -import -alias anna_berg -file Anna_Berg.crt -keystore clientkeystore -storepass password
keytool -import -alias johan_svensson -file Johan_Svensson.crt -keystore clientkeystore -storepass password
keytool -import -alias eva_johansson -file Eva_Johansson.crt -keystore clientkeystore -storepass password
keytool -import -alias jonas_gustavsson -file Jonas_Gustavsson.crt -keystore clientkeystore -storepass password
keytool -import -alias malin_larsson -file Malin_Larsson.crt -keystore clientkeystore -storepass password
keytool -import -alias anders_andersson -file Anders_Andersson.crt -keystore clientkeystore -storepass password
keytool -import -alias lisa_lindgren -file Lisa_Lindgren.crt -keystore clientkeystore -storepass password
keytool -import -alias sven_svensson -file Sven_Svensson.crt -keystore clientkeystore -storepass password
keytool -import -alias socialstyrelsen -file Socialstyrelsen.crt -keystore clientkeystore -storepass password

# Create a server-side keystore and truststore
keytool -genkey -alias server -keyalg RSA -keystore serverkeystore -storepass password
keytool -certreq -alias server -keystore serverkeystore -file server.csr -storepass password
echo 03 > ca.srl

#openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key -out server.crt \
#   -subj "/CN=Server" -CAcreateserial -days 365
openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key -out server.crt -CAcreateserial -days 365

keytool -import -alias ca -file ca.crt -keystore serverkeystore -storepass password
keytool -import -alias server -file server.crt -keystore serverkeystore -storepass password
keytool -import -alias server -file ca.crt -keystore servertruststore -storepass password
