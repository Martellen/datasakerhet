public class MedicalRecord {
    private String division;
    private User doctor;
    private User patient;
    private User nurse;
    // private static int count = 0;
    private int id;
    private String notes;
    // private Random rand;

    public MedicalRecord(User doctor, User nurse, User patient, String division, int id, String notes){
        this.doctor = doctor;
        this.patient = patient;
        this.nurse = nurse;
        this.division = division;
        this.id = id;
        this.notes = notes;
    }

    public int getId() {
        return id;
    }

    public String getNotes() {
        return notes;
    }

    public String getDivision() {
        return division;
    }

    public User getPatient() {
        return patient;
    }

    public User getDoctor() {
        return doctor;
    }

    public User getNurse() {
        return nurse;
    }

    public void appendNotes(String newnotes) {
        notes += "||" + newnotes;
    }

    public String toString() {
        return "Record ID: " + id + "\tPatient: " + patient.getName() + "\tDoctor: " + doctor.getName()
                + "\tNurse: " + nurse.getName() + "\tDivision: " + division + "\tNotes:" + notes;
    }

    public String toStringFile() {
        return doctor.getAlias() + ";" + nurse.getAlias() + ";" + patient.getAlias() + ";" + division + ";" + id + ";" + notes;
    }
}
