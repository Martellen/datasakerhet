import java.util.ArrayList;
import java.io.*;

public class DataBase {

    private ArrayList<User> userList;
    private ArrayList<MedicalRecord> records;
    private int size;

    public DataBase() {
        userList = new ArrayList<>();
        records = new ArrayList<>();
    }

    public User getUser(String alias) {
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getAlias().equals(alias)) {
                return userList.get(i);
            }
        }
        return null;
    }

    public void addRecord(MedicalRecord medRec) {
        records.add(medRec);
    }
    public void addUser(User user) {
        userList.add(user);
    }

    public MedicalRecord getRecord(int id) {
        for (int i = 0; i < records.size(); i++) {
            if (records.get(i).getId() == id) {
                return records.get(i);
            }
        }
        return null;
    }

    public ArrayList<MedicalRecord> getRecords(String alias, String role) {
        ArrayList<MedicalRecord> result = new ArrayList<>();
        switch (role) {
            case "nurse": {
                for (int i = 0; i < records.size(); i++) {
                    User nurse = records.get(i).getNurse();
                    if (nurse.getAlias().equals(alias) || nurse.getDivision().equals(records.get(i).getDivision())) {
                        result.add(records.get(i));
                    }
                }
            }
            case "doctor": {
                for (int i = 0; i < records.size(); i++) {
                    User doctor = records.get(i).getDoctor();
                    if (doctor.getAlias().equals(alias) || doctor.getDivision().equals(records.get(i).getDivision())) {
                        result.add(records.get(i));
                    }
                }
            }
            case "patient": {
                for (int i = 0; i < records.size(); i++) {
                    User nurse = records.get(i).getPatient();
                    if (nurse.getAlias().equals(alias)) {
                        result.add(records.get(i));
                    }
                }
            }
            case "agency": {
                return records;
            } 
        }
        return result;
    }

    public ArrayList<MedicalRecord> getRecordsFromPatient(String alias, String role) {
        ArrayList<MedicalRecord> result = new ArrayList<>();
        switch (role) {
            case "nurse": {
                for (int i = 0; i < records.size(); i++) {
                    User nurse = records.get(i).getNurse();
                    if (nurse.getAlias().equals(alias) || nurse.getDivision().equals(records.get(i).getDivision())) {
                        result.add(records.get(i));
                    }
                }
            }
            case "doctor": {
                for (int i = 0; i < records.size(); i++) {
                    User doctor = records.get(i).getDoctor();
                    if (doctor.getAlias().equals(alias) || doctor.getDivision().equals(records.get(i).getDivision())) {
                        result.add(records.get(i));
                    }
                }
            }
            case "patient": {
                for (int i = 0; i < records.size(); i++) {
                    User nurse = records.get(i).getPatient();
                    if (nurse.getAlias().equals(alias)) {
                        result.add(records.get(i));
                    }
                }
            }
            case "agency": {
                return records;
            } 
        }
        return result;
    }

    public Boolean removeRec(Integer id) {
        for(int i = 0; i < records.size(); i++) {
            if (records.get(i).getId() == id) {
                records.remove(i);
                return true;
            }
        }
        return false;
    }

    public int getMaxId() {
        return records.size();
    }

    /*
     * Writes all records to file 'records'. 
     * Call this function whenever changes to records are made
     */
    public void writeRecords() {
        try {
            File file = new File("records");
            FileWriter writeempty = new FileWriter(file, false);
            writeempty.write("");
            writeempty.flush();
            writeempty.close();
            FileWriter writer = new FileWriter(file, true);
            for (var record : records) {
                writer.write(record.toStringFile());
                writer.write("\n");
            }
            writer.flush();
            writer.close();
            
        } catch (IOException e) {
            System.out.println("Error writing to records file");
            e.printStackTrace();
        }
    }

    /*
     * Checks if alias is in the passwordfile
     */
    public Boolean checkUserAlias(String alias) {
        try {
            File file = new File("database"); // replace with your file name
            FileInputStream fileStream = new FileInputStream(file);
            InputStreamReader input = new InputStreamReader(fileStream);
            BufferedReader reader = new BufferedReader(input);

            String line = reader.readLine();
            while (line != null) {
                if (line.split(";")[0].equals(alias)) {
                    return true;
                }
                line = reader.readLine();
            }
            reader.close();
            input.close();
            fileStream.close();
        } catch (Exception e) {
            System.out.println("Error reading passwordfile: ");
        }
        
        return false;
    } 

    /*
     * Checks if password for a given alias is correct
     */
    public Boolean checkUserPassword(String alias, String hashedPassword) {
        try {
            File file = new File("database"); // replace with your file name
            FileInputStream fileStream = new FileInputStream(file);
            InputStreamReader input = new InputStreamReader(fileStream);
            BufferedReader reader = new BufferedReader(input);
            String line = reader.readLine();
            while (line != null) {
                String[] l = line.split(";");
                if (l[0].equals(alias)) {
                    if (l[1].equals(hashedPassword)) {
                        return true;
                    }
                }
                line = reader.readLine();
            }
            reader.close();
            input.close();
            fileStream.close();
        } catch (Exception e) {
            System.out.println("Error reading passwordfile: ");
        }
        
        return false;
    }
}
