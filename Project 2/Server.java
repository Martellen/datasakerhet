import java.io.*;
import java.net.*;
import javax.net.*;
import javax.net.ssl.*;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.logging.Level;

public class Server implements Runnable {
  private ServerSocket serverSocket = null;
  private static int numConnectedClients = 0;
  private static DataBase dataBase = new DataBase();
  private AuditLog log;

  public Server(ServerSocket ss) throws IOException {
    serverSocket = ss;
    newListener();

    log = new AuditLog("log");
    log.logger.setLevel(Level.INFO);
  }

  public void run() {
    try {
      
      SSLSocket socket = (SSLSocket) serverSocket.accept();
      newListener();
      SSLSession session = socket.getSession();
      Certificate[] cert = session.getPeerCertificates();

      System.out.println("Cipher suite: " + session.getCipherSuite() + "\n");

      String s = ((X509Certificate) cert[0]).getSubjectX500Principal().getName();
      System.out.println("Certificate recieved from client: " + s + "\n");

      PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
      BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

      /*
       * Verifies alias and password against passwordfile, unlimited tries until correct alias and password is entered
       */
      String alias = null;
      while ((alias = in.readLine()) != null) {
        if (dataBase.checkUserAlias(alias)) {
          System.out.println("Alias verified");
          out.println("> Alias verified, enter password:");
          out.flush();
          String hashedPassword = null;
          while ((hashedPassword = in.readLine()) != null) {
            if (dataBase.checkUserPassword(alias, hashedPassword)) {
              System.out.println("Password verified");
              out.println("> Password verified, welcome " + alias + "!");
              out.flush();
              break;
            } 
            else {
              System.out.println("Password incorrect");
              out.println("> Password incorrect, try again!");
              out.flush();
              hashedPassword = null;
            }
          }
          break;
        }
      }

      numConnectedClients++;
      System.out.println("\nclient connected");
      System.out.println("client name: " + alias);
      System.out.println(numConnectedClients + " concurrent connection(s)\n");
      Actions actions = new Actions(dataBase, dataBase.getUser(alias), log);

      String clientMsg = null;
      while ((clientMsg = in.readLine()) != null) {
        String result = actions.performAction(clientMsg);
        System.out.println("received '" + clientMsg + "' from client");
        //System.out.print("sending '" + result + "' to client...");
        //actions.performAction(clientMsg);
        out.println(result);
        out.flush();
        System.out.println("done\n");
      }
      in.close();
      out.close();
      socket.close();
      numConnectedClients--;
      System.out.println("client disconnected");
      System.out.println(numConnectedClients + " concurrent connection(s)\n");
    } catch (IOException e) {
      System.out.println("Client died: " + e.getMessage());
      e.printStackTrace();
      return;
    }
  }

  private void newListener() {
    (new Thread(this)).start();
  } // calls run()

  public static void main(String args[]) {
    createDatabase();
    createMedicalRecord();
    System.out.println("\nServer Started\n");
    int port = -1;
    if (args.length >= 1) {
      port = Integer.parseInt(args[0]);
    }
    String type = "TLSv1.2";
    try {
      ServerSocketFactory ssf = getServerSocketFactory(type);
      ServerSocket ss = ssf.createServerSocket(port);
      ((SSLServerSocket) ss).setNeedClientAuth(true); // enables client authentication
      new Server(ss);
    } catch (IOException e) {
      System.out.println("Unable to start Server: " + e.getMessage());
      e.printStackTrace();
    }
  }

  private static void createDatabase() {
    System.out.println("\nInitilazing Database\n");
    BufferedReader reader;
    
    try {
      reader = new BufferedReader(new FileReader("database"));

      String line = reader.readLine();
      while (line != null) {
        String[] info = line.split(";");
        User newUser = new User(info[4], info[3], info[2], info[0]);
        dataBase.addUser(newUser);
        // read next line
        line = reader.readLine();
      }

      reader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void createMedicalRecord() {
    System.out.println("Creating records");
    BufferedReader reader;
    
    try {
      reader = new BufferedReader(new FileReader("records"));

      String line = reader.readLine();
      while (line != null) {
        String[] info = line.split(";");
        User doctor = dataBase.getUser(info[0]);
        User nurse = dataBase.getUser(info[1]);
        User patient = dataBase.getUser(info[2]);
        String division = info[3];
        int id = Integer.parseInt(info[4]);
        String notes = info[5];

        MedicalRecord record = new MedicalRecord(doctor, nurse, patient,division, id, notes);
        dataBase.addRecord(record);
        // read next line
        line = reader.readLine();
      }

      reader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static ServerSocketFactory getServerSocketFactory(String type) {
    if (type.equals("TLSv1.2")) {
      SSLServerSocketFactory ssf = null;
      try { // set up key manager to perform server authentication
        SSLContext ctx = SSLContext.getInstance("TLSv1.2");
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
        KeyStore ks = KeyStore.getInstance("JKS");
        KeyStore ts = KeyStore.getInstance("JKS");
        char[] password = "password".toCharArray();
        // keystore password (storepass)
        ks.load(new FileInputStream("util/serverkeystore"), password);
        // truststore password (storepass)
        ts.load(new FileInputStream("util/servertruststore"), password);
        kmf.init(ks, password); // certificate password (keypass)
        tmf.init(ts); // possible to use keystore as truststore here
        ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
        ssf = ctx.getServerSocketFactory();
        return ssf;
      } catch (Exception e) {
        e.printStackTrace();
      }
    } else {
      return ServerSocketFactory.getDefault();
    }
    return null;
  }
}
