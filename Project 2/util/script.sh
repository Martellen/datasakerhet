# CA certificate
openssl req -x509 -sha256 -nodes -days 365 -newkey RSA -keyout ca.key -out ca.crt -subj "/CN=CA"

# Import CA certificate into clienttruststore
keytool -import -alias ca -file ca.crt -keystore clienttruststore -storepass password

# Generate a new key-pair for the client and store it in clientkeystore
# Generate keys for three nurses
keytool -genkey -alias Maria_Olsson -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Maria Olsson, OU=ONH, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"
keytool -genkey -alias Anna_Berg -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Anna Berg, OU=ONH, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"
keytool -genkey -alias Johan_Svensson -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Johan Svensson, OU=ONH, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"

# Generate keys for three patients
keytool -genkey -alias Anders_Andersson -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Anders Andersson, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"
keytool -genkey -alias Lisa_Lindgren -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Lisa Lindgren, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"
keytool -genkey -alias Sven_Svensson -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Sven Svensson, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"

# Generate keys for three doctors
keytool -genkey -alias Eva_Johansson -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Eva Johansson, OU=Oncology, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"
keytool -genkey -alias Jonas_Gustavsson -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Jonas Gustavsson, OU=Cardiology, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"
keytool -genkey -alias Malin_Larsson -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Malin Larsson, OU=Pediatrics, O=Lunds Universitetsjukhus, L=Lund, ST=Skane, C=SE"

# Generate key for Socialstyrelsen
keytool -genkey -alias Socialstyrelsen -keyalg RSA -keystore clientkeystore -storepass password \
    -dname "CN=Socialstyrelsen, ST=Sweden, C=SE"


# Generate a certificate signing request for the clients public key and store it in clientkeystore
keytool -certreq -alias Maria_Olsson -keystore clientkeystore -file Maria_Olsson.csr -storepass password 
keytool -certreq -alias Anna_Berg -keystore clientkeystore -file Anna_Berg.csr -storepass password 
keytool -certreq -alias Johan_Svensson -keystore clientkeystore -file Johan_Svensson.csr -storepass password 

keytool -certreq -alias Eva_Johansson -keystore clientkeystore -file Eva_Johansson.csr -storepass password
keytool -certreq -alias Jonas_Gustavsson -keystore clientkeystore -file Jonas_Gustavsson.csr -storepass password 
keytool -certreq -alias Malin_Larsson -keystore clientkeystore -file Malin_Larsson.csr -storepass password

keytool -certreq -alias Anders_Andersson -keystore clientkeystore -file Anders_Andersson.csr -storepass password
keytool -certreq -alias Lisa_Lindgren -keystore clientkeystore -file Lisa_Lindgren.csr -storepass password
keytool -certreq -alias Sven_Svensson -keystore clientkeystore -file Sven_Svensson.csr -storepass password

keytool -certreq -alias Socialstyrelsen -keystore clientkeystore -file Socialstyrelsen.csr -storepass password

#Generate a digital certificate for the clients public key using the CSR§
echo 01 > ca.srl
openssl x509 -req -in Maria_Olsson.csr -CA ca.crt -CAkey ca.key -out Maria_Olsson.crt -CAcreateserial -days 365
openssl x509 -req -in Anna_Berg.csr -CA ca.crt -CAkey ca.key -out Anna_Berg.crt -CAcreateserial -days 365
openssl x509 -req -in Johan_Svensson.csr -CA ca.crt -CAkey ca.key -out Johan_Svensson.crt -CAcreateserial -days 365

openssl x509 -req -in Eva_Johansson.csr -CA ca.crt -CAkey ca.key -out Eva_Johansson.crt -CAcreateserial -days 365
openssl x509 -req -in Jonas_Gustavsson.csr -CA ca.crt -CAkey ca.key -out Jonas_Gustavsson.crt -CAcreateserial -days 365
openssl x509 -req -in Malin_Larsson.csr -CA ca.crt -CAkey ca.key -out Malin_Larsson.crt -CAcreateserial -days 365

openssl x509 -req -in Anders_Andersson.csr -CA ca.crt -CAkey ca.key -out Anders_Andersson.crt -CAcreateserial -days 365
openssl x509 -req -in Lisa_Lindgren.csr -CA ca.crt -CAkey ca.key -out Lisa_Lindgren.crt -CAcreateserial -days 365
openssl x509 -req -in Sven_Svensson.csr -CA ca.crt -CAkey ca.key -out Sven_Svensson.crt -CAcreateserial -days 365

openssl x509 -req -in Socialstyrelsen.csr -CA ca.crt -CAkey ca.key -out Socialstyrelsen.crt -CAcreateserial -days 365

# Import certificate chain into clientkeystore
keytool -import -alias ca -file ca.crt -keystore clientkeystore -storepass password
keytool -import -alias Maria_Olsson -file Maria_Olsson.crt -keystore clientkeystore -storepass password
keytool -import -alias Anna_Berg -file Anna_Berg.crt -keystore clientkeystore -storepass password
keytool -import -alias Johan_Svensson -file Johan_Svensson.crt -keystore clientkeystore -storepass password
keytool -import -alias Eva_Johansson -file Eva_Johansson.crt -keystore clientkeystore -storepass password
keytool -import -alias Jonas_Gustavsson -file Jonas_Gustavsson.crt -keystore clientkeystore -storepass password
keytool -import -alias Malin_Larsson -file Malin_Larsson.crt -keystore clientkeystore -storepass password
keytool -import -alias Anders_Andersson -file Anders_Andersson.crt -keystore clientkeystore -storepass password
keytool -import -alias Lisa_Lindgren -file Lisa_Lindgren.crt -keystore clientkeystore -storepass password
keytool -import -alias Sven_Svensson -file Sven_Svensson.crt -keystore clientkeystore -storepass password
keytool -import -alias Socialstyrelsen -file Socialstyrelsen.crt -keystore clientkeystore -storepass password

# Create a server-side keystore and truststore
keytool -genkey -alias server -keyalg RSA -keystore serverkeystore -storepass password
keytool -certreq -alias server -keystore serverkeystore -file server.csr -storepass password
echo 03 > ca.srl

#openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key -out server.crt \
#   -subj "/CN=Server" -CAcreateserial -days 365
openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key -out server.crt -CAcreateserial -days 365

keytool -import -alias ca -file ca.crt -keystore serverkeystore -storepass password
keytool -import -alias server -file server.crt -keystore serverkeystore -storepass password
keytool -import -alias server -file ca.crt -keystore servertruststore -storepass password
