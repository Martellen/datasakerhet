import java.io.File;
import java.io.IOException;
import java.util.logging.*;

public class AuditLog {
    public Logger logger;
    FileHandler fh;

    public AuditLog(String fileName) throws SecurityException, IOException {
        File file = new File(fileName);
        if (!file.exists()){
            file.createNewFile();
        }

        fh = new FileHandler(fileName, true);
        logger = Logger.getLogger("log");
        logger.addHandler(fh);
        logger.setUseParentHandlers(false);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);
    }

    public  void logging (String log){
        logger.log(Level.INFO, log);
    }
}
