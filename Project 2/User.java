public class User {

    private String fullName;
    private String division;
    private String role;
    private String alias;

    public User(String fullName, String role, String div, String alias){
        this.fullName = fullName;
        this.role = role;
        this.division = div;
        this.alias = alias;
    }
    public String getName() {
        return this.fullName;
    }

    public String getAlias() {
        return this.alias;
    }

    public String getDivision() {
        if(this.division != null) {
            return this.division;
        }
        return "No division set yet";
    }

    // get authority level?

    public String getRole() {
        if(this.role != null) {
            return this.role;
        }
        return "No role set yet";
    }
}
