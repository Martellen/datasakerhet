openssl genpkey -algorithm RSA -out ca.key -aes256
openssl req -key ca.key -new -x509 -out ca.crt -subj "/CN=CA"
keytool -import -alias ca -file ca.crt -keystore clienttruststore -storepass password
keytool -genkey -alias client -keyalg RSA -keystore clientkeystore -storepass password
keytool -certreq -alias client -keystore clientkeystore -file client.csr -storepass password
echo 01 > ca.srl
openssl x509 -req -in client.csr -CA ca.crt -CAkey ca.key -out client.crt -CAcreateserial -days 365
keytool -import -alias ca -file ca.crt -keystore clientkeystore -storepass password
keytool -import -alias client -file client.crt -keystore clientkeystore -storepass password

keytool -genkey -alias server -keyalg RSA -keystore serverkeystore -storepass password
keytool -certreq -alias server -keystore serverkeystore -file server.csr -storepass password
echo 03 > ca.srl
openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key -out server.crt -CAcreateserial -days 365
keytool -import -alias ca -file ca.crt -keystore serverkeystore -storepass password
keytool -import -alias server -file server.crt -keystore serverkeystore -storepass password
keytool -import -alias server -file ca.crt -keystore servertruststore -storepass password

#To use for CN field
#Eric Martell (er3586ma-s) Philip Nielsen (ph3883ni-s) Sara Saber (sa7006sa-s) Baqer Khafaja (ba5484kh-s)